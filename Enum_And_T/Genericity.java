package Enum;
/*
 * 回顾继承向上转型/向下转型
 */
public class Genericity{
	private Object obj;

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
	public static void main(String[] args) {
		Genericity man=new Genericity();
		man.setObj(new Boolean(true));//向上转型
		System.out.println(man.getObj());//向下转型
		man.setObj(new Float(3.456));
		System.out.println(man.getObj());
		
		try{
			double s=(double) man.getObj();//父类非子类实例报错
		}catch(Exception e) {
			e.printStackTrace();
		}
		float f=(float)man.getObj();//父类对象非子类实例
		System.out.println(f);
	}

}
