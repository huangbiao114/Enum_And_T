package Enum;
	/*
	 * 将get方法放进接口中
	 * 枚举类型实现接口,重写接口中的方法
	 */
	interface Enum{
		String getS();
		int getI();
	}
public class EnumConstructorsTest2 {
		enum container implements Enum{
			container_A{
				@SuppressWarnings("unused")
				public String getS() {
					return "枚举类型container_A";
				}
				@SuppressWarnings("unused")
				public int getI(){
					return 2;
				}
			},
			container_B{
				@SuppressWarnings("unused")
				public String getS() {
					return "枚举类型container_B";
				}
				@SuppressWarnings("unused")
				public int getI() {
					return 4;
				}
			},
			container_C{
				@SuppressWarnings("unused")
				public String getS() {
					return "枚举类型container_C";
				}
				@SuppressWarnings("unused")
				public int getI() {
					return 5;
				}
			},
			container_D{
				@SuppressWarnings("unused")
				public String getS() {
					return "枚举类型container_D";
				}
				@SuppressWarnings("unused")
				public int getI() {
					return 7;
				}
			};
		}
		//private static int i=5;
		public static void main(String[] args) {
			for(int u=0;u<container.values().length;u++) {
				System.out.println(container.values()[u].getS());
			}
		}
}







