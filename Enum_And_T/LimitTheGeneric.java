package Enum;
/*
 * 泛型的各种限制
 */
import java.util.*;

public class LimitTheGeneric<T extends List> {//LimitTheGeneric对象只能时继承List的类型
	static LimitTheGeneric <? extends List> a=null;//泛型通配符向下限制
	public static void main(String[] args) {
		Map<String,String> map=new HashMap();
		LimitTheGeneric<ArrayList> man1 = new LimitTheGeneric<ArrayList>();
		LimitTheGeneric<LinkedList> man2 = new LimitTheGeneric<LinkedList>();
		//LimitTheGeneric<HashMap> man3=new LimitTheGeneric<HashMap>();
		//错误:没有继承List集合
		a=new LimitTheGeneric<ArrayList>();
		a=new LimitTheGeneric<LinkedList>();
		//a=new LimitTheGeneric<HashMap>();错误,没有继承List集合
		class A<T>{
			A<? super List> a=null;//泛型通配符向上限制
			public void man() {
				a=new A<List>();
				a=new A<Object>();
				//a=new A<ArrayList>();错误,ArrayList不是List父类
			}
		}
	}
}
