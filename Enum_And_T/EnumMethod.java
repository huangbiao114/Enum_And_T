package Enum;
/*
 * Enum数组与value(),valueOf(),compareTo()方法
 */
public class EnumMethod {
		 enum container{
			container_A,container_B,container_C,container_D;
		}
		 static container[] a=container.values();//获取所有枚举常量并将之放进Enum数组中
		 public static void valueOf1(container s) {
		 for(int i=0;i<a.length;i++) {
			 int b=s.compareTo((container) a[i]);
			 /*
			  * 将参数(a[i))的常量与指定枚举常量(s)比较
			  * 在前面返回整型1
			  * 后面返回-1
			  * 相等返回0
			  */
			 System.out.println(b);
		 }
		 }
		 public static void main(String[] args) {
			 for(int j=0;j<a.length;j++) {
				 System.out.print(a[j].ordinal()+",");
				 System.out.println(a[j]);
			 }
			 System.out.println("container_C的索引值为;"+container.container_C.ordinal());
			 valueOf1(container.valueOf("container_B"));
			 /*遍历方法二
			  * for(int i=0;i<container.values().length;i++) {
				System.out.println(container.values()[i]);
			}*/
		}
}
