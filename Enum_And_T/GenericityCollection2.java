package Enum;
/*
 * ArrayList集合/TreeSet集合/HashMap集合/Vector集合
 */
import java.util.*;

public class GenericityCollection2 {
		public static void main(String[] args) {
			System.out.println("ArrayList集合");
			List <String>list=new ArrayList<String>();
			list.add("胡歌");
			list.add("依晨");
			list.add("霍建华");
			list.add("汪涵");
			list.add("何炅");
			list.set(4, "萧炎");//改变集合中的值
			Iterator <String>ter1=list.iterator();
			while(ter1.hasNext()) {
				String s1=ter1.next();
				System.out.println(s1);
			}
			System.out.println("TreeSet集合");
			Set <Integer> set=new TreeSet();
			set.add(12);
			set.add(23);
			set.add(34);
			set.add(45);
			Iterator <Integer> ter2=set.iterator();
			while(ter2.hasNext()) {
				Integer a=ter2.next();
				System.out.println(a);
			}
			System.out.println("HashMap集合");
			Map <String,Boolean>map=new HashMap<>();
			String []b= {"地球在变大??","宇宙在膨胀吗?","恒星寿终时变成红巨星?","恒星寿终一段时间后坍缩形成的第一个星体为黑洞?"};
			boolean []c= {true,true,true,false};
			for(int i=0;i<b.length;i++) {
				map.put(b[i], c[i]);
			}
			Iterator <String>ter3=map.keySet().iterator();
			while(ter3.hasNext()) {
				String d=(String)ter3.next();
				Boolean f=(Boolean)map.get(d);
				System.out.println(d+","+f);
			}
			System.out.println("Vecter集合");
			Vector <String> ver=new Vector<String>();
			String[]g= {"我是你?","你是他?","他是不是我?"};
			for(int j=0;j<g.length;j++) {
				ver.add("成员:"+g[j]);//将指定组件添加进容器末尾
			}
			ver.set(2, "成员:他是我");
			System.out.println(ver.toString());
			Iterator<String> ter4=ver.iterator();
			while(ter4.hasNext()) {
				String y=(String)ter4.next();
				System.out.println(y);
			}
		}
}
