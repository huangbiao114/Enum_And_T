package Enum;
/*
 * 枚举类型的构造方法
 */
public class EnumConstructorsTest {
		enum container{
			container_A("我是狮子"),
			container_B("我是虱子"),
			container_C("我是老虎"),
			container_D(4);
			private String description;
			private int a;
		
		private container() {
			
		}
		private container(String description) {
			this.description=description;
		}
		private container(int a) {
			this.a=a;
		}
		public String getdescription() {
			return description;
		}
		
		public int getA() {
			return a;
		}
		
		
		}
		public static void main(String[] args) {
			for(int e=0;e<container.values().length-1;e++) {
				System.out.println(container.values()[e].getdescription());
			}
			System.out.println(container.valueOf("container_D").getA());
		}
}
