package Enum;
/*
 * 遍历泛型数组
 */
public class GenericityArray<T> {
		private T[] array;//定义一个数组类型
		public void setArray(T[] array) {
			this.array=array;
		}
		public T[] getArray() {
			return array;
		}
		public static void main(String[] args) {
			GenericityArray<String> man=new GenericityArray<String>();
			String [] array=new String[] {"1","2","3"};
			man.setArray(array);
			for(String b:man.getArray()) {
				System.out.println(b);
			}
		}
}
