package Enum;
/*
 * 泛型多元化
 */
public class GenericityTest1 <T>{//定义泛型类
		private T age;
		
		public T getAge() {
			return age;
		}

		public void setAge(T age) {
			this.age = age;
		}

		public static void main(String[] args) {
			//实例泛型类为一个布尔包装型对象
			GenericityTest1<Boolean> man1=new GenericityTest1<Boolean>();
			//实力泛型类为一个float包装型对象
			GenericityTest1<Float> man2=new GenericityTest1<>();
			man1.setAge(true);
			System.out.println(man1.getAge());
			man2.setAge(23.5f);
			System.out.println(man2.getAge());
		}
}
