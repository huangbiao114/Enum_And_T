package Enum;

/*
 *泛型集合类[键-K ,键值-V]
 *
 */
import java.util.*;

public class GenericityCollection1<K, V> {
	private Map<K, V> man = new HashMap<K, V>();// 创建集合对象,HashMap集合无序高效

	public void setMap(K k, V v) {
		man.put(k, v);
	}

	public V getMap(K k) {
		return man.get(k);
	}

	public static void main(String[] args) {
		GenericityCollection1<String, Integer> man2 = new GenericityCollection1<String, Integer>();
		man2.setMap("胡歌", 35);
		man2.setMap("林依晨", 35);
		man2.setMap("霍建华", 38);
		System.out.println(man2.man.get("胡歌"));// 返回指定键对应的值
		System.out.println(man2.man.keySet());// 返回所有键的set集合

		Collection<Integer> b = man2.man.values();// 将所有的键值放进Collcetion集合中
		System.out.println("键值集合b遍历数组获取键值");
		/*
		 * 键值集合b遍历数组获取键值
		 */
		Object[] b2 = b.toArray();// 将集合转换成Object数组
		for (int a = 0; a < b2.length; a++) {// 遍历键值数组
			System.out.println(b2[a]);
		}
		/*
		 * 迭代器迭代键值集合b获取键值
		 */
		System.out.println("迭代器迭代键值集合b获取键值");
		Iterator<Integer> c = b.iterator();
		while (c.hasNext()) {
			Integer d = c.next();
			System.out.println(d);
		}
		/*
		 * 键集合s遍历数组获取键值
		 */
		System.out.println("键集合s遍历数组获取键值");
		Set<String> s = man2.man.keySet();
		Object[] f = s.toArray();// 循环输出键
		for (int g = 0; g < f.length; g++) {
			System.out.println(man2.man.get(f[g]));
		}
		/*
		 * 迭代器迭代键集合s获取键值
		 */
		System.out.println("迭代器迭代键集合s获取键值");
		Iterator<String> w = s.iterator();
		while (w.hasNext()) {
			String e = w.next();
			System.out.println(man2.man.get(e));
		}

	}
}
