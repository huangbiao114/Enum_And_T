package Enum;
/*
 * 枚举类型与接口常量
 */
interface container {//创建接口定义常量
	public static final int a = 1;
	public static final int b = 3;
}

public class EnumTest {
	enum container2 {//创建枚举类型并添加常量
		container_A, container_B, container_C;
	}

	public static void doit(int m) {//定义一个参数为接口常量的方法
		if (m == container.a) {
			System.out.println("方法1传入接口常量a");
		}
		if (m == container.b) {
			System.out.println("方法2传入接口常量b");
		}
	}

	public static void doit2(container2 n) {//创建一个参数为Enum枚举常量的方法
		if (n == container2.container_A) {
			System.out.println("方法2传入枚举container_A");
		}
		if (n == container2.container_B) {
			System.out.println("方法2传入枚举container_B");
		}
	}

	public static void main(String[] args) {
		EnumTest.doit(container.b);
		EnumTest.doit2(container2.container_B);
		EnumTest.doit2(container2.container_A);
		EnumTest.doit(2);

	}

}
